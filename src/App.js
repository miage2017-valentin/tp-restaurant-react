import React, { Component } from 'react';
import './App.css';
import ListRestaurant from './components/ListRestaurant';
import EditFormRestaurant from './components/EditFormRestaurant';
import AddFormRestaurant from './components/AddFormRestaurant';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import AddIcon from '@material-ui/icons/Add';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';

const styles = theme => ({
  fab: {
    position: 'absolute',
    top: theme.spacing.unit * 2,
    left: theme.spacing.unit * 2,
  }
});

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      valueTab: 0,
      msgOpen: false,
      message: ''
    };
  }

  showEdit = (editedRestaurant) => {
    // this.panelAction.showEditForm(editedRestaurant);
    console.log(this.editModal);
    this.editModal.handleOpen(editedRestaurant);
  }

  showAddForm = () => {
    this.addModal.handleOpen();
  }

  refreshList = () => {
    console.log(this.listRestaurant);
    this.listRestaurant.refreshList();
  }

  showMessage = (message) => {
    this.setState({ message, msgOpen: true });
  };

  hideMessage = () => {
    this.setState({ msgOpen: false });
  }

  render() {
    const {classes} = this.props;
    return (
      <div className="App">
        <Paper>
          <Button className={classes.fab} variant="fab" color="primary" aria-label="Add" onClick={this.showAddForm}>
            <AddIcon />
          </Button>
          <AddFormRestaurant onRef={comp => this.addModal = comp}
            showMessage={this.showMessage}
            refresh={this.refreshList} />
          <ListRestaurant showEdit={this.showEdit} 
            onRef={comp => { this.listRestaurant = comp; }} 
            showMessage={this.showMessage} />
          <EditFormRestaurant onRef={comp => this.editModal = comp}
            refresh={this.refreshList}
            showMessage={this.showMessage} />
          <Snackbar
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'left',
            }}
            open={this.state.msgOpen}
            autoHideDuration={3000}
            onClose={this.hideMessage}
            ContentProps={{
              'aria-describedby': 'message-id',
            }}
            message={<span id="message-id">{this.state.message}</span>}
            action={[
              <IconButton
                key="close"
                aria-label="Close"
                color="inherit"
                onClick={this.hideMessage}
              >
                <CloseIcon />
              </IconButton>,
            ]}
          />
        </Paper>
      </div>
    );
  }
}

export default withStyles(styles)(App);
