import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import DeleteIcon from '@material-ui/icons/Delete';
import CreateIcon from '@material-ui/icons/Create';
import IconButton from '@material-ui/core/IconButton';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import LastPageIcon from '@material-ui/icons/LastPage';
import TableHead from '@material-ui/core/TableHead';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import _ from 'lodash';
import { myConfig } from '../config';


const actionsStyles = theme => ({
    root: {
        flexShrink: 0,
        color: theme.palette.text.secondary,
        marginLeft: theme.spacing.unit * 2.5,
    },
});

class TablePaginationActions extends React.Component {
    handleFirstPageButtonClick = event => {
        this.props.onChangePage(event, 0);
    };

    handleBackButtonClick = event => {
        this.props.onChangePage(event, this.props.page - 1);
    };

    handleNextButtonClick = event => {
        this.props.onChangePage(event, this.props.page + 1);
    };

    handleLastPageButtonClick = event => {
        this.props.onChangePage(
            event,
            Math.max(0, Math.ceil(this.props.count / this.props.rowsPerPage) - 1),
        );
    };

    render() {
        const { classes, count, page, rowsPerPage, theme } = this.props;

        return (
            <div className={classes.root}>
                <IconButton
                    onClick={this.handleFirstPageButtonClick}
                    disabled={page === 0}
                    aria-label="Première Page"
                >
                    {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
                </IconButton>
                <IconButton
                    onClick={this.handleBackButtonClick}
                    disabled={page === 0}
                    aria-label="Page précédente"
                >
                    {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
                </IconButton>
                <IconButton
                    onClick={this.handleNextButtonClick}
                    disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                    aria-label="Prochaine Page"
                >
                    {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
                </IconButton>
                <IconButton
                    onClick={this.handleLastPageButtonClick}
                    disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                    aria-label="Dernière Page"
                >
                    {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
                </IconButton>
            </div>
        );
    }
}

TablePaginationActions.propTypes = {
    classes: PropTypes.object.isRequired,
    count: PropTypes.number.isRequired,
    onChangePage: PropTypes.func.isRequired,
    page: PropTypes.number.isRequired,
    rowsPerPage: PropTypes.number.isRequired,
    theme: PropTypes.object.isRequired,
};

const TablePaginationActionsWrapped = withStyles(actionsStyles, { withTheme: true })(
    TablePaginationActions,
);

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
    },
    table: {
        minWidth: 500,
    },
    tableWrapper: {
        overflowX: 'auto',
    },
    textField: {
        marginLeft: 'auto',
        marginRight: 'auto',
    },
    button: {
        margin: 'auto',
    }
});


class CustomPaginationActionsTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            rows: [],
            page: 0,
            rowsPerPage: 5,
            count: 0,
            emptyRows: 0,
            searchName: ""
        }
    }

    refreshList = () => {
        let urlGet = myConfig.urlAPI + '/api/restaurants?page=' + this.state.page + '&pagesize=' + this.state.rowsPerPage;
        let urlCount = myConfig.urlAPI + '/api/restaurants/count';
        if (this.state.searchName !== '') {
            urlGet += '&name=' + this.state.searchName;
            urlCount += '?name=' + this.state.searchName;
        }
        this.countRestaurants(urlCount);
        this.getRestaurants(urlGet);
    }

    countRestaurants(url) {
        fetch(url)
            .then(response => {
                response.json().then(res => {
                    this.elementCount = res.data;
                    this.setState({
                        count: res.data
                    });
                })
            })
            .catch(function (err) {
                console.log(err);
            });
    }

    getRestaurants(url) {
        fetch(url)
            .then(response => {
                response.json().then(res => {
                    let emptyRows = this.state.rowsPerPage - Math.min(this.state.rowsPerPage, res.data.length);
                    this.setState({
                        rows: res.data,
                        emptyRows: emptyRows
                    });
                })
            })
            .catch(err => {
                console.log("erreur dans le get : " + err)
            });
    }

    handleChangePage = (event, page) => {
        this.setState({ page }, this.refreshList);
    };

    handleChangeRowsPerPage = event => {
        this.setState({ rowsPerPage: event.target.value }, this.refreshList);
    };

    removeRestaurant(id, index) {
        let url = myConfig.urlAPI + "/api/restaurants/" + id;

        fetch(url, {
            method: "DELETE",
        })
            .then((responseJSON) => {
                this.refreshList();
                this.props.showMessage('Ce restaurant a été supprimé');
            })
            .catch(function (err) {
                console.log(err);
            });
    }

    showEditRestaurant(item) {

    }
    handleChange = name => event => {
        let restaurant = this.state.selectedRestaurant;
        restaurant[name] = event.target.value;
        this.setState({
            selectedRestaurant: restaurant,
        });
    };

    search = _.debounce((value) => {
        this.setState({ page: 0 }, this.refreshList);
    }, 100);

    handleChangeSearch(value) {
        this.setState({ searchName: value });
    }

    componentDidMount = () => {
        this.refreshList();
        this.props.onRef(this);
    };


    render() {
        const { classes } = this.props;
        return (
            <div>
                <Grid container
                    alignItems="center">
                    <TextField
                        id="standard-search"
                        label="Rechercher avec le nom"
                        margin="normal"
                        type="search"
                        className={classes.textField}
                        value={this.state.searchName}
                        onChange={(event) => { this.setState({ searchName: event.target.value }); this.search(event.target.value) }}
                    />
                </Grid>
                <div className={this.props.tableWrapper}>
                    <Table className={this.props.table}>
                        <TableHead>
                            <TableRow>
                                <TableCell>Nom</TableCell>
                                <TableCell numeric>Cusine</TableCell>
                                <TableCell numeric>Action</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {this.state.rows.map(row => {
                                return (
                                    <TableRow key={row._id}>
                                        <TableCell component="th" scope="row">
                                            {row.name}
                                        </TableCell>
                                        <TableCell numeric>{row.cuisine}</TableCell>
                                        <TableCell numeric>
                                            <IconButton onClick={() => { this.props.showEdit(row); }}>
                                                <CreateIcon />
                                            </IconButton>
                                            <IconButton onClick={() => { this.removeRestaurant(row._id) }}>
                                                <DeleteIcon />
                                            </IconButton>
                                        </TableCell>
                                    </TableRow>
                                );
                            })}
                            {this.state.emptyRows > 0 && (
                                <TableRow style={{ height: 48 * this.state.emptyRows }}>
                                    <TableCell colSpan={6} />
                                </TableRow>
                            )}
                        </TableBody>
                        <TableFooter>
                            <TableRow>
                                <TablePagination
                                    colSpan={3}
                                    count={this.state.count}
                                    rowsPerPage={this.state.rowsPerPage}
                                    page={this.state.page}
                                    onChangePage={this.handleChangePage}
                                    onChangeRowsPerPage={this.handleChangeRowsPerPage}
                                    ActionsComponent={TablePaginationActionsWrapped}
                                />
                            </TableRow>
                        </TableFooter>
                    </Table>
                </div>
            </div>
        );
    }
}

CustomPaginationActionsTable.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CustomPaginationActionsTable);