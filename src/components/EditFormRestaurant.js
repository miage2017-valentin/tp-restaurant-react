import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import PropTypes from 'prop-types';
import { myConfig } from '../config';

function getModalStyle() {
    const top = 50;
    const left = 50;

    return {
        top: `${top}%`,
        left: `${left}%`,
        transform: `translate(-${top}%, -${left}%)`,
    };
}

const styles = theme => ({
    paper: {
        position: 'absolute',
        width: theme.spacing.unit * 50,
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing.unit * 4,
    },
});


class EditFormRestaurant extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            open: false,
            selectedRestaurant: {
                _id: "",
                name: "",
                cuisine: ""
            }
        }
    }

    handleChange = name => event => {
        let restaurant = this.state.selectedRestaurant;
        restaurant[name] = event.target.value;
        this.setState({
            selectedRestaurant: restaurant,
        });
        console.log(this.state.selectedRestaurant)
    };

    handleOpen = (editedRestaurant) => {
        this.setState({ open: true, selectedRestaurant: editedRestaurant});
    };

    handleClose = () => {
        this.setState({ open: false });
    };

    componentDidMount = () => {
        this.props.onRef(this);
    };

    editRestaurant = () => {
        console.log(this.props);
        if (!this.state.selectedRestaurant.name || !this.state.selectedRestaurant.cuisine) {
            return;
        }
        this.colMod = false;
        this.modifMod = false;
        this.selectedForModification = null
        let url = myConfig.urlAPI + "/api/restaurants/" + this.state.selectedRestaurant._id;
        let form = new FormData();
        form.append('nom', this.state.selectedRestaurant.name);
        form.append('cuisine', this.state.selectedRestaurant.cuisine)
        fetch(url, {
            method: "PUT",
            body: form
        })
            .then((responseJSON) => {
                this.props.refresh();
                this.handleClose();
                this.props.showMessage('Ce restaurant a été modifié');
            })
            .catch(function (err) {
                console.log(err);
            });
    }

    render() {
        const { classes } = this.props;

        return (
            <Modal
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                open={this.state.open}
                onClose={this.handleClose}
            >
                <div style={getModalStyle()} className={classes.paper}>
                    <Grid container
                        alignItems="center"
                        direction="column"
                        alignItems="center">
                        <Grid container item
                            alignItems="center"
                            xs={6}>
                            <TextField
                                disabled
                                id="standard-disabled"
                                label="Id"
                                margin="normal"
                                className={classes.textField}
                                value={this.state.selectedRestaurant._id}
                                onChange={this.handleChange('_id')}
                            />
                        </Grid>
                        <Grid container item
                            alignItems="center"
                            xs={6}>
                            <TextField
                                id="standard-name"
                                label="Name"
                                margin="normal"
                                className={classes.textField}
                                value={this.state.selectedRestaurant.name}
                                onChange={this.handleChange('name')}
                            />
                        </Grid>
                        <Grid container item xs={6}>
                            <TextField
                                id="standard-name"
                                label="Cuisine"
                                margin="normal"
                                className={classes.textField}
                                value={this.state.selectedRestaurant.cuisine}
                                onChange={this.handleChange('cuisine')}
                            />
                        </Grid>
                        <Grid container item xs={6}>
                            <Button onClick={() => { this.editRestaurant() }}
                                variant="contained"
                                color="primary" className={classes.button}>
                                Modifier
                            </Button>
                        </Grid>
                    </Grid>;
                </div>
            </Modal>
        );
    }
}

EditFormRestaurant.propTypes = {
    classes: PropTypes.object.isRequired,
};


export default withStyles(styles)(EditFormRestaurant);