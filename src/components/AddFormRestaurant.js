import React from 'react';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import PropTypes from 'prop-types';
import { myConfig } from '../config';

function getModalStyle() {
    const top = 50;
    const left = 50;

    return {
        top: `${top}%`,
        left: `${left}%`,
        transform: `translate(-${top}%, -${left}%)`,
    };
}

const styles = theme => ({
    paper: {
        position: 'absolute',
        width: theme.spacing.unit * 50,
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing.unit * 4,
    },
});

class AddFormRestaurant extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            open: false,
            newRestaurant: {
                name: '',
                cuisine: ''
            }
        }
    }

    handleOpen = () => {
        this.setState({ open: true });
    };

    handleClose = () => {
        this.setState({ open: false });
    };

    componentDidMount = () => {
        this.props.onRef(this);
    };

    handleChange = name => event => {
        let restaurant = this.state.newRestaurant;
        restaurant[name] = event.target.value;
        this.setState({
            newRestaurant: restaurant,
        });
    };

    addRestaurant() {
        if (!this.state.newRestaurant.name || !this.state.newRestaurant.cuisine) {
            return;
        }
        this.colMod = false;
        this.insertMod = false;
        let url = myConfig.urlAPI + "/api/restaurants"
        let form = new FormData();
        form.append('nom', this.state.newRestaurant.name);
        form.append('cuisine', this.state.newRestaurant.cuisine)
        fetch(url, {
            method: "POST",
            body: form
        })
            .then((responseJSON) => {
                responseJSON.json()
                    .then((res) => {
                        this.props.refresh();
                        this.handleClose();
                        this.props.showMessage('Ce restaurant a été ajouté');
                    });
            })
            .catch(function (err) {
                console.log(err);
            });
    }

    render() {
        const { classes } = this.props;
        return <Modal
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
            open={this.state.open}
            onClose={this.handleClose}
        >
            <div style={getModalStyle()} className={classes.paper}>
                <Grid container
                    alignItems="center"
                    direction="column"
                    alignItems="center">
                    <Grid container item
                        alignItems="center"
                        xs={6}>
                        <TextField
                            id="standard-name"
                            label="Name"
                            margin="normal"
                            className={classes.textField}
                            value={this.state.newRestaurant.name}
                            onChange={this.handleChange('name')}
                        />
                    </Grid>
                    <Grid container item xs={6}>
                        <TextField
                            id="standard-name"
                            label="Cuisine"
                            margin="normal"
                            className={classes.textField}
                            value={this.state.newRestaurant.cuisine}
                            onChange={this.handleChange('cuisine')}
                        />
                    </Grid>
                    <Grid container item xs={6}>
                        <Button onClick={() => { this.addRestaurant() }}
                            variant="contained"
                            color="primary" className={classes.button}>
                            Ajouter
                    </Button>
                    </Grid>
                </Grid>;
        </div>
        </Modal>;
    }
}

AddFormRestaurant.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(AddFormRestaurant);